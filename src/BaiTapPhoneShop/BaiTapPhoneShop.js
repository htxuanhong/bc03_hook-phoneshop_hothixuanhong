import React, { useState } from "react";
import Cart from "./Cart";
import dataPhone from "./dataPhone";
import Detail from "./Detail";
import ProductList from "./ProductList";

export default function BaiTapPhoneShop(props) {
  const [productList] = useState(dataPhone);
  const [selectedItem, setSelectedItem] = useState(null);
  const [carts, setCarts] = useState([]);

  const _handleThemSanPham = (sanPham) => {
    let cloneGioHang = [...carts];
    let index = cloneGioHang.findIndex((item) => {
      return item.maSP === sanPham.maSP;
    });
    if (index === -1) {
      let newSanPham = { ...sanPham, soLuong: 1 };
      cloneGioHang.push(newSanPham);
    } else {
      cloneGioHang[index].soLuong++;
    }

    setCarts(cloneGioHang);
  };

  const _handleItemClick = (item) => {
    setSelectedItem(item);
  };

  const _handleXoaSanPham = (maSanPham) => {
    let cloneGioHang = [...carts];
    let index = cloneGioHang.findIndex((item) => {
      return item.maSP === maSanPham;
    });
    if (index !== -1) {
      cloneGioHang.splice(index, 1);
      setCarts(cloneGioHang);
    }
  };
  const _handleUpDown = (maSanPham, giaTri) => {
    let cloneGioHang = [...carts];
    let index = cloneGioHang.findIndex((item) => {
      return item.maSP === maSanPham;
    });
    if (index !== -1) {
      cloneGioHang[index].soLuong += giaTri;
    }
    cloneGioHang[index].soLuong === 0 && cloneGioHang.splice(index, 1);

    setCarts(cloneGioHang);
  };

  return (
    <div className="py-3">
      <h3 className="text-success">Cửa hàng PhoneShop xin chào!</h3>
      <div className="text-right">
        <button
          type="button"
          class="btn btn-outline-danger mb-2  "
          data-toggle="modal"
          data-target="#exampleModal"
        >
          Giỏ hàng ({carts.length})
        </button>

        {carts.length > 0 && (
          <Cart
            handleXoaSanPham={_handleXoaSanPham}
            gioHang={carts}
            handleUpDown={_handleUpDown}
          />
        )}
      </div>

      <ProductList
        handleItemClick={_handleItemClick}
        handleThemSanPham={_handleThemSanPham}
        productList={productList}
      />

      <Detail item={selectedItem} />
    </div>
  );
}
