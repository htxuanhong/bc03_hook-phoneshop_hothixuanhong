import React, { Component } from "react";

export default class ProductItem extends Component {
  render() {
    const { handleItemClick } = this.props;

    let { tenSP, hinhAnh } = this.props.data;

    return (
      <div className=" col-4  ">
        <div className="card">
          <div className="h-75  pt-3">
            <img
              src={hinhAnh}
              className="card-img-top  "
              style={{ "max-height": "330px" }}
              alt=""
            />
          </div>
          <div className="card-body h-25 text-left">
            <h5 className="card-title">{tenSP}</h5>

            <button
              className="btn btn-success mr-3"
              onClick={() => handleItemClick(this.props.data)}
            >
              Xem chi tiết
            </button>
            <button
              className="btn btn-danger"
              onClick={() => {
                this.props.handleThemSanPham(this.props.data);
              }}
            >
              Thêm vào giỏ hàng
            </button>
          </div>
        </div>
      </div>
    );
  }
}
